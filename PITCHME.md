---?image=assets/image/main-bg.jpg
## Mobius 2018 Piter</span>
Дмитрий Солдатов

---

## Что это такое?

- 400+ участников <!-- .element: class="fragment" -->
- 30+ спикеров <!-- .element: class="fragment" -->
- 3 параллельных трека докладов <!-- .element: class="fragment" -->
- 20 докладов по Android <!-- .element: class="fragment" -->

---

## О чём я расскажу

- краткий обзор докладов <!-- .element: class="fragment" -->
- практические советы от спикеров <!-- .element: class="fragment" -->
- рекомендации к просмотру <!-- .element: class="fragment" -->

---

### Нелегкая дорога до JobScheduler и обратно
<br/>
<br/>
#### Артур Василов
<br/>
Яндекс

---

##### Нелегкая дорога до JobScheduler и обратно

- опыт перехода на minSdk 21 <!-- .element: class="fragment" -->
- недокументированные проблемы с JobScheduler на Android 5 и 6 <!-- .element: class="fragment" -->
- неочевидные нюансы API <!-- .element: class="fragment" -->
- глобальный try ... catch на любой вызов JobScheduler <!-- .element: class="fragment" -->
- возвращение к AlarmManager на API < 24 <!-- .element: class="fragment" -->
- JobIntentService <!-- .element: class="fragment" -->

---

### Скрипач не нужен: отказываемся от RxJava в пользу корутин в Kotlin
<br/>
<br/>
#### Владимир Иванов
<br/>
EPAM Systems

---

##### Скрипач не нужен: отказываемся от RxJava в пользу корутин в Kotlin

- основные понятия и принципы работы корутин <!-- .element: class="fragment" -->
- "прямой" стиль кода, обработка ошибок в try ... catch <!-- .element: class="fragment" -->
- сравнение по количеству созданных объектов <!-- .element: class="fragment" -->
- сравнение сложности стектрейсов <!-- .element: class="fragment" -->
- взаимодействие Rx и корутин <!-- .element: class="fragment" -->
- тестирование корутин <!-- .element: class="fragment" -->
- каналы, как замена Subject <!-- .element: class="fragment" -->

---

#### Рождение, жизнь и смерть, или
#### Что происходит с приложением в системе
<br/>
<br/>
#### Антон Дудаков
<br/>
Яндекс

---

###### Рождение, жизнь и смерть, или Что происходит с приложением в системе

- загрузка ОС и старт приложения <!-- .element: class="fragment" -->
- жизненный цикл Activity <!-- .element: class="fragment" -->
- взаимодействие компонентов и приложений <!-- .element: class="fragment" -->
- если STICKY сервис падал за последнюю минуту, его перезапустят через 1/4/16с<!-- .element: class="fragment" -->
- ManifestBan: если дважды упасть в onReceive, приложение больше не получит бродкасты до следующего запуска<!-- .element: class="fragment" -->

---

### Reverse engineering mobile apps: how, why, and what now?
<br/>
<br/>
#### Michał Kałużny
<br/>
NWTN Berlin

---

##### Reverse engineering mobile apps: how, why, and what now?

- статический и динамический анализ <!-- .element: class="fragment" -->
- baksmali, JDGUI, Frida, APKTool <!-- .element: class="fragment" -->
- сравнение инструментов для Android и iOS <!-- .element: class="fragment" -->
- демонстрация модификации бинарника (Android/iOS) <!-- .element: class="fragment" -->
- обфускаторы не защитят от специалиста <!-- .element: class="fragment" -->
- хитрости для хранения токенов <!-- .element: class="fragment" -->

---

### На плечах гигантов: языки, у которых учился Kotlin
<br/>
<br/>
#### Андрей Бреслав
<br/>
JetBrains

---

##### На плечах гигантов: языки, у которых учился Kotlin

- Java, C#, Scala, ML, ... <!-- .element: class="fragment" -->
- брать хорошие идеи - не стыдно <!-- .element: class="fragment" -->
- важно - решить, что НЕ добавлять в язык <!-- .element: class="fragment" -->
- для тернарного оператора нужны ? и : <!-- .element: class="fragment" -->
- syntactic whitespace <!-- .element: class="fragment" -->
- вещи, за которые пользователь платит, должны быть явными <!-- .element: class="fragment" -->
- object : Foo() - цена за отсутствие оператора new <!-- .element: class="fragment" -->

---

### Мультиплатформенная архитектура на Kotlin для iOS и Android
<br/>
<br/>
#### Роман Яцына, Иван Важнов
<br/>
Revolut

---

##### Мультиплатформенная архитектура на Kotlin для iOS и Android

- сэмпл - загрузка и отображение списка <!-- .element: class="fragment" -->
- переиспользование моделей и бизнес слоя <!-- .element: class="fragment" -->
- зависимости между модулями через expect/actual <!-- .element: class="fragment" -->
- самодельные корутины на iOS <!-- .element: class="fragment" -->
- цель достигнута <!-- .element: class="fragment" -->
- страдания без тулинга <!-- .element: class="fragment" -->

---

### Как не состариться во время сборки: Kapt и другие приключения
<br/>
<br/>
#### Денис Неклюдов
<br/>
90seconds.tv, Android GDE

---

##### Как не состариться во время сборки: Kapt и другие приключения

- ускорение clean build с 3,5 мин до 1,5 мин <!-- .element: class="fragment" -->
- каждая фича - в своём модуле <!-- .element: class="fragment" -->
- core и app максимально тонкие <!-- .element: class="fragment" -->
- Gradle 4: implements & api <!-- .element: class="fragment" -->
- параллельная сборка независимых модулей <!-- .element: class="fragment" -->
- сложности с dagger-android и subcomponents <!-- .element: class="fragment" -->
- ссылки по теме <!-- .element: class="fragment" -->

---

### Одной строкой

- Trusting iOS SDKs (Felix Krause, Google) <!-- .element: class="fragment" -->

- А/B-тестирование, которое вам понравится (Константин Волков, Revolut) <!-- .element: class="fragment" -->

- Upload в Oдноклассниках (Кирилл Попов, Одноклассники) <!-- .element: class="fragment" -->
   
- What mom never told you about multithreading (Fernando Cejas, IBM) <!-- .element: class="fragment" -->
   
---

### Ещё одной строкой

- Сервисы: нельзя без них, а как с ними жить? (Йонатан Левин, KolGene) <!-- .element: class="fragment" -->
   
- Акторные модели: новый взгляд на старый подход (Владимир Теблоев, Сбербанк-Технологии) <!-- .element: class="fragment" -->

- Релизы мобильных приложений в Avito (Алексей Шпирко, Avito) <!-- .element: class="fragment" -->

- Многомодульная архитектура проекта (Евгений Суворов, Avito) <!-- .element: class="fragment" -->
   
---

### За кадром

- Add some reality to your app with ArCore (Yuliya Kaleda, Jet.com)

- Вспомнить всех: Autofill Framework и Account Transfer API (Кирилл Борисов, Яндекс)

- Микросервисная архитектура при разработке Android multimedia device (Иван Алякскин, EPAM Systems)

---

### За кадром

- Введение в AOSP, или Как потратить ночь на сборку Android (Виктор Лапин, Advantum)
  
- Профайлинг в примерах: ищем бутылочное горлышко (Артур Бадретдинов, Vyng)

---

## Must see

1. Нелегкая дорога до JobScheduler и обратно (Артур Василов, Яндекс)

2. Скрипач не нужен: отказываемся от RxJava в пользу корутин в Kotlin (Владимир Иванов, EPAM Systems)

3. Рождение, жизнь и смерть, или Что происходит с приложением в системе (Антон Дудаков, Яндекс)

---

### Questions?
